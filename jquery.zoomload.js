/*
 * jQuery.ZoomLoad.js - jQuery plugin for zoom loading images
 *
 * Copyright (c) 2007-2015 Qietu inc
 *
 * Licensed under the MIT license:
 *   http://www.opensource.org/licenses/mit-license.php
 *
 * Project home:
 *   http://www.qietu.com/p/jquery_zoomload
 *
 * Version:  1.0.0
 *
 */


(function($){
    $.fn.zoomload = function(){
		
		var obj = $(this);
		var w = obj.width();
		var h = obj.height();
		var l = obj.css('margin-left') == 'auto' ? 0 : parseInt(obj.css('margin-left'));
		var r = obj.css('margin-right') == 'auto' ? 0 : parseInt(obj.css('margin-right'));
		var t = obj.css('margin-top') == 'auto' ? 0 : parseInt(obj.css('margin-top'));
		var b = obj.css('margin-bottom') == 'auto' ? 0 : parseInt(obj.css('margin-bottom'));
	
	
		
		obj.each(function(){
			var top=$(window).scrollTop();
			$(this).each(function(){
				
				$(this).wrap('<div class="zoomload" style="width:'+ w +'px; height:'+ h +'px;"></div>');
				
				if($(this).offset().top >= top){
					$(this).css({'opacity':0, 'width':1, 'height':1,'margin-left':'+='+w/2, 'margin-top':'+='+h/2});	
				}
				else{
					$(this).addClass('animated');	
				}
				
										  
			})			  
		});
		
		
		/*var init = function(){
			 alert(3);
			obj.each(function(){
							 
				alert(4);		  
			})
			
		}
		
		init();*/
		
		function update(){
			
			var top=$(window).scrollTop();
			var i = 0;
			obj.each(function(){
				
				
				
				//n = $(this).index();
				
				//console.log($(this).offset().top);
					
			
				
				if(!$(this).is(':animated') && !$(this).hasClass('animated')){
					
					if ($(this).offset().top -top< $(window).height()){
						
						i++;
						
						//console.log(222);
						
						
						$(this).addClass('animated').delay(i*200).animate({'opacity':1,'width':w+20, 'height':h+20, 'margin-left': l-10, 'margin-top': t-10}, 500 ,function(){
							$(this).stop().animate({'width':w, 'height':h,'margin-left': l, 'margin-top': t},500);																																								 						});	
					};		
				}
												  
						  
			})
		}
		
		$(window).bind('scroll', update).resize(update);
		update();
		
	}
})(jQuery);





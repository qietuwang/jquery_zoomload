#jquery_zoomload

jQuery.ZoomLoad是一款基于jQuery开发的插件，它可以让你的网站图片以放大的形式打开，并且支持滚动条的响应，可以增强用户体验，并且调用很简单，一行代码调用就可以让你的网站炫起来。PS：它与lazyload不一样，它不具备替网站提速的“功效”，它只是在图片的打开方式上让用户获得一种全新的体验。

演示文档：http://www.qietu.com/p/jquery_zoomload

更新记录

v1.1: 2015.1.26 更新了算法，修复图片带border，padding属性，导致错位的bug <br/>
v1.1: 2015.1.26 更改了zoomload外围的display属性为 inline-block (img的display一样) <br/>
v1.1: 2015.1.26 手机等移动设备下禁用插件<br/>
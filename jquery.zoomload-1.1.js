/*
 * jQuery.ZoomLoad.js - jQuery plugin for zoom loading images
 *
 * Copyright (c) 2007-2015 Qietu inc
 *
 * Licensed under the MIT license:
 *   http://www.opensource.org/licenses/mit-license.php
 *
 * Project home:
 *   http://www.qietu.com/p/jquery_zoomload
 *
 * Version:  1.0.0
 * changelog
 * 20150126 更新了算法，修复图片带border，padding属性，导致错位的bug
            更改了zoomload外围的display属性为 inline-block (img的display一样)
    		手机等移动设备下禁用插件
 *
 */


(function($){
	

	
	$.fn.zoomload = function(){
		
		
		var ismobile = parseInt($(window).width()) < 900 ? true : false;
	
		
		var obj = $(this);
		
		
		obj.each(function(){
			
			
			
		
			var top=$(window).scrollTop();
			$(this).each(function(){
								  
				if(!$(this).is(':visible')){
					return false;	
				}
				
				if(ismobile){
					return false;	
				}
								  
				w = $(this).width();
				h = $(this).height();
				l = $(this).css('margin-left') == 'auto' ? 0 : parseInt($(this).css('margin-left'));
				r = $(this).css('margin-right') == 'auto' ? 0 : parseInt($(this).css('margin-right'));
				t = $(this).css('margin-top') == 'auto' ? 0 : parseInt($(this).css('margin-top'));
				b = $(this).css('margin-bottom') == 'auto' ? 0 : parseInt($(this).css('margin-bottom'));
			
			$(this).attr({'w':w, 'h':h, 'l':l, 'r':r, 't':t, 'b':b});
				
				ww = parseInt($(this).width()) + parseInt($(this).css('padding-left')) + parseInt($(this).css('padding-right')) + parseInt($(this).css('border-left-width')) + parseInt($(this).css('border-right-width'));
				hh = parseInt($(this).height()) + parseInt($(this).css('padding-top')) + parseInt($(this).css('padding-bottom')) + parseInt($(this).css('border-top-width')) + parseInt($(this).css('border-bottom-width'));
				
				$(this).wrap('<span class="zoomload" style="width:'+ ww +'px; height:'+ hh +'px; overflow:hidden; display:inline-block;"></span>');
				
				if($(this).offset().top >= top){
					$(this).css({'opacity':0, 'width':1, 'height':1,'margin-left':'+='+w/2, 'margin-top':'+='+h/2});	
				}
				else{
					$(this).addClass('animated');	
				}
				
										  
			})			  
		});
		
		
		/*var init = function(){
			 alert(3);
			obj.each(function(){
							 
				alert(4);		  
			})
			
		}
		
		init();*/
		
		function update(){
			
			if(!$(this).is(':visible')){
					return false;	
				}
				if(ismobile){
					return false;	
				}

			
			var top=$(window).scrollTop();
			var i = 0;
			obj.each(function(){
				
				
				var w = parseInt($(this).attr('w'));
				var h = parseInt($(this).attr('h'));
				var l = parseInt($(this).attr('l'));
				var r = parseInt($(this).attr('r'));
				var t = parseInt($(this).attr('t'));
				var b = parseInt($(this).attr('b'));
				
				//n = $(this).index();
				
				//console.log($(this).offset().top);
					
			
				
				if(!$(this).is(':animated') && !$(this).hasClass('animated')){
					
					if ($(this).offset().top -top< $(window).height()){
						
						i++;
						
						//console.log(222);
						
						
						$(this).addClass('animated').delay(i*200).animate({'opacity':1,'width':w+20, 'height':h+20, 'margin-left': l-10, 'margin-top': t-10}, 500 ,function(){
							$(this).stop().animate({'width':w, 'height':h,'margin-left': l, 'margin-top': t},500);																																								 						});	
					};		
				}
												  
						  
			})
		}
		
		$(window).bind('scroll', update).resize(update);
		update();
		
	}
})(jQuery);




